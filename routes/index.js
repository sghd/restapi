var express = require('express');
var router = express.Router();
var Post=require('../models/Post');
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});


router.get('/posts',function (req,res,next) {
    Post.find({},function (err,data) {
       res.json(data);
    });
});

router.post('/posts',function (req,res,next) {
  var p=new Post(req.body);
    p.save(function (err,new_post) {
        res.json(new_post);
    });
});
router.patch('/posts/:id',function (req,res,next) {
   Post.findOneAndUpdate(req.params.id,req.body,{new:true},function (err,post) {
      if(err) next(err);
       res.json(post);
   });
});
router.delete('/posts/:id',function (req,res,next) {
   Post.remove(req.params.id,function (err,p) {
      res.json({message:"Successful Deleted!!"});
   });
});

router.get('/posts/:id',function (req,res,next) {
   Post.findOne({_id:req.params.id},function (err,post){
     res.json(post);
   });
});











module.exports = router;
