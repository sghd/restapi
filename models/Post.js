/**
 * Created by datht on 5/5/17.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;


var PostSchema = new Schema({
    title: {
        type: String,
        Required: 'Kindly enter the title of the post'
    },
    link:String,
    content:String,
    created_date: {
        type: Date,
        default: Date.now
    }
});

module.exports = mongoose.model('Post',PostSchema);